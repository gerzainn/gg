package dev.com.toreritoapp;

import android.content.Intent;
import android.graphics.pdf.PdfDocument;
import android.os.Environment;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class Login extends AppCompatActivity {

    private Button loginbtn;
    private  int codeRead = 102;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        loginbtn = findViewById(R.id.btnLogin);
        loginbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent downloadIntent = new Intent(getApplicationContext(), Account.class);
                startActivity(downloadIntent);
            }
        });

    }



    public void  crearpdf()

    {
        PdfDocument document = new PdfDocument();
        PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder(400, 600, 1).create();

        PdfDocument.Page page = document.startPage(pageInfo);
        Canvas canvas = page.getCanvas();
        Paint paint = new Paint();

        paint.setColor(Color.BLACK);
        canvas.drawText("DEMANDA URGENTE CONFORME AL ARTÍCULO 15 DE LA LEY DE AMPARO", 80, 80, paint);

        //  terminar la página

        document.finishPage(page);

        String directory_path = Environment.getExternalStorageDirectory().getAbsolutePath()+ "/mypdf/";
        File file = new File(directory_path);


        if (!file.exists())
        {
            file.mkdir();
        }


        String targetPdf = directory_path+"test.pdf";
        File filePath = new File(targetPdf);
        try {
            document.writeTo(new FileOutputStream(filePath));
            Toast.makeText(this, "Se creo el pdf", Toast.LENGTH_LONG).show();
        } catch (IOException e) {
            Log.e("main", "error "+e.toString());
            Toast.makeText(this, "Ocurrio un error: " + e.toString(),  Toast.LENGTH_LONG).show();
        }

        document.close();





    }


}
