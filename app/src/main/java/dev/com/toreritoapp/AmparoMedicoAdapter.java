package dev.com.toreritoapp;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import dev.com.toreritoapp.model.AmparoMedico;

public class AmparoMedicoAdapter extends FirebaseRecyclerAdapter<AmparoMedico,AmparoMedicoAdapter.ViewHolder>
{

    /**
     * Initialize a {@link RecyclerView.Adapter} that listens to a Firebase query. See
     * {@link FirebaseRecyclerOptions} for configuration options.
     *
     * @param options
     */
    public AmparoMedicoAdapter(@NonNull FirebaseRecyclerOptions<AmparoMedico> options) {
        super(options);
    }

    @Override
    public void onDataChanged() {
        super.onDataChanged();
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull AmparoMedico model) {
        switch (model.getTipo())
        {
            case 1 :
                holder.direccion.setText(R.string.amparoadulto);
                holder.nombrepersona.setText(model.getNombre());

                break;
            case  3 :
                holder.direccion.setText(R.string.amparomenor);
                holder.nombremenor.setVisibility(View.VISIBLE);
                holder.nombremenor.setText(model.getNombremenor());
                holder.nombrepersona.setText(model.getNombretutor());
                break;
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_amparo,parent,false);

        return new ViewHolder(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        public TextView nombrepersona;
        public ImageView amparo;
        public  TextView direccion;
        public TextView fecha;
        public TextView nombremenor;
        public ViewHolder(@NonNull View itemView)
        {
            super(itemView);
            nombrepersona=itemView.findViewById(R.id.tv_amparo);
            amparo= itemView.findViewById(R.id.iv_amparolista);
            nombremenor = itemView.findViewById(R.id.tv_nombremenor);
            direccion=itemView.findViewById(R.id.tv_direccionotificacion);
            fecha=itemView.findViewById(R.id.tv_fechamparo);
        }
    }
}
