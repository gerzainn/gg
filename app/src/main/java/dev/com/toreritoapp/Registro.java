package dev.com.toreritoapp;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.AppCompatCheckBox;

import android.os.Handler;
import android.telephony.PhoneNumberUtils;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import androidx.constraintlayout.widget.ConstraintLayout;
import dev.com.toreritoapp.model.User;

public class Registro extends AppCompatActivity implements  ActionBottomDialogUser.ItemClickListener {
    private DatabaseReference mFirebaseDatabase;
    private FirebaseDatabase mFirebaseInstance;
    private FirebaseAuth firebaseAuth;
    private ConstraintLayout contenedor;
    private TextInputEditText nombre,email,password, lastname, phone;
    private TextInputLayout inputLayoutnombre, inputLayoutapellido, inputLayoutemail, inputLayoutpassword, inputLayoutphone;
    private Button btnregistro;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        mFirebaseInstance = FirebaseDatabase.getInstance();
        mFirebaseDatabase = mFirebaseInstance.getReference("users");
        firebaseAuth      = FirebaseAuth.getInstance();


        AppCompatCheckBox checkterms;
        nombre= findViewById(R.id.edt_nombre);
        email = findViewById(R.id.edt_email);
        password= findViewById(R.id.edt_pasword);
        lastname = findViewById(R.id.edt_apellidos);
        phone = findViewById(R.id.edt_telefono);
        btnregistro= findViewById(R.id.btn_registro);
        inputLayoutnombre= findViewById(R.id.textinputnombre);
        inputLayoutapellido= findViewById(R.id.textinputapellidos);
        inputLayoutemail= findViewById(R.id.textinputemail);
        inputLayoutphone= findViewById(R.id.textinputphone);
        inputLayoutpassword= findViewById(R.id.textinputpassword);
        checkterms=findViewById(R.id.check_terms);
        TextView textViewterminos = findViewById(R.id.tv_terminos);
        TextView textViewprivacidad = findViewById(R.id.tv_privacidad);
        contenedor=findViewById(R.id.contenedor_registro);

        textViewterminos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               DialogoTerminos();
            }
        });

        textViewprivacidad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogoPrivacidad();
            }
        });

        nombre.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length() > 0)
                {
                    Log.d("Registro",s.toString());

                }

            }

            @Override
            public void afterTextChanged(Editable s)
            {

            }
        });

        email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(Patterns.EMAIL_ADDRESS.matcher(s).matches())
                {
                    email.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_check_ok, 0);

                }
                else{
                    email.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_warning, 0);
                }
            }
        });

        password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length() > 8)
                {
                    btnregistro.setBackgroundResource(R.drawable.button_background_red);
                }
                else
                {
                    btnregistro.setBackgroundResource(R.drawable.btn_background_gray);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        phone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(Patterns.PHONE.matcher(s).matches())
                {
                    phone.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_check_ok, 0);

                }
                else{
                    phone.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_warning, 0);
                }

            }
        });



        checkterms.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(buttonView.isChecked())
                {
                    if(validar())
                    {
                        btnregistro.setBackgroundResource(R.drawable.button_background_red);
                    }

                }
                else
                {
                    btnregistro.setBackgroundResource(R.drawable.btn_background_gray);
                }
            }
        });

        btnregistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createUser();

            }
        });

    }

    public void createUser()
    {

        if(!validar()) {
            return;
        }

            String useremail= email.getText().toString().trim();
            String pass = password.getText().toString().trim();

            firebaseAuth.createUserWithEmailAndPassword(useremail,pass)
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e)
                        {
                            Toast.makeText(getApplicationContext(),e.getMessage(),Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if(task.isSuccessful())
                            {
                                Log.d("Registro","correcto");
                                onAuthSucess(Objects.requireNonNull(task.getResult().getUser()));

                                    styleSnackbar(contenedor);

                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        finish();
                                    }
                                },6000L);

                            }
                            else
                            {
                                Log.e("Registro","error ");
                            }
                        }

                    });

    }

    public void showBottomSheet() {
        ActionBottomDialogUser actionBottomDialogUser =
                ActionBottomDialogUser.newInstance();
        actionBottomDialogUser.show(getSupportFragmentManager(),
                ActionBottomDialogUser.TAG);
    }

    public void  DialogoTerminos()
    {
        AlertDialog.Builder builder= new AlertDialog.Builder(this);
        builder.setTitle(R.string.terminos_titulo);
        builder.setMessage(R.string.terminostexto);
        builder.setPositiveButton(R.string.aceptar, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();

    }

    public void  DialogoPrivacidad()
    {
        AlertDialog.Builder builder= new AlertDialog.Builder(this);
        builder.setTitle(R.string.privacidad_titulo);
        builder.setMessage(R.string.avisoprivacidad_texto);
        builder.setPositiveButton(R.string.aceptar, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();

    }

    public void styleSnackbar(View view) {
            Snackbar snackbar = Snackbar.make(view, getString(R.string.usuarioagregado), Snackbar.LENGTH_LONG)
                    .setAction("", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                        }
                    });

        //Accion
        snackbar.setActionTextColor(getResources().getColor(R.color.whiteTextColor));
        View snackBarView = snackbar.getView();
        //Fondo
        snackBarView.setBackgroundColor(getResources().getColor(R.color.verde));

        snackbar.show();
    }
    public boolean validar()
    {
        boolean valid = true;
        String  name  = nombre.getText().toString();
        if(TextUtils.isEmpty(name)) {

            valid = false;
            inputLayoutnombre.setError(getString(R.string.validarnombre));
            btnregistro.setBackgroundResource(R.drawable.btn_background_gray);
            btnregistro.setEnabled(false);
        }
        else
        {
            inputLayoutnombre.setError(null);
            btnregistro.setEnabled(true);
            btnregistro.setBackgroundResource(R.drawable.button_background_red);
        }
        String apellidos =  lastname.getText().toString();
        if(TextUtils.isEmpty(apellidos)) {
            valid = false;
            inputLayoutapellido.setError(getString(R.string.validarapellido));
            btnregistro.setBackgroundResource(R.drawable.btn_background_gray);
            btnregistro.setEnabled(false);

        }
        else {
            inputLayoutnombre.setError(null);
            btnregistro.setEnabled(true);
            btnregistro.setBackgroundResource(R.drawable.button_background_red);
        }
        String correo= email.getText().toString().trim();
        if(TextUtils.isEmpty(correo)) {
            valid = false;
            inputLayoutemail.setError(getString(R.string.validaremail));
            btnregistro.setBackgroundResource(R.drawable.btn_background_gray);
            btnregistro.setEnabled(false);

        }
        else
        {
            inputLayoutemail.setError(null);
            btnregistro.setEnabled(true);
            btnregistro.setBackgroundResource(R.drawable.button_background_red);
        }
        String contrase = password.getText().toString().trim();
        if(TextUtils.isEmpty(contrase))
        {
            valid=false;
            inputLayoutpassword.setError(getString(R.string.password_error));
            btnregistro.setBackgroundResource(R.drawable.btn_background_gray);
            btnregistro.setEnabled(false);
        }
        else
        {
            inputLayoutpassword.setError(null);
            btnregistro.setEnabled(true);
            btnregistro.setBackgroundResource(R.drawable.button_background_red);
        }
        String number = phone.getText().toString();

        if(TextUtils.isEmpty(number))
        {
            valid=false;
            inputLayoutphone.setError("Por favor , ingresa un numero");
            btnregistro.setBackgroundResource(R.drawable.btn_background_gray);
            btnregistro.setEnabled(false);
        }
        else
        {
            TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            String countryIso = telephonyManager.getNetworkCountryIso().toUpperCase();
            phone.setText(PhoneNumberUtils.formatNumber(number, countryIso));
            inputLayoutpassword.setError(null);
            btnregistro.setEnabled(true);
            btnregistro.setBackgroundResource(R.drawable.button_background_red);
        }

          return valid;
    }

    public   void onAuthSucess(FirebaseUser user)
    {
        String username = nombre.getText().toString();
        String use_id = user.getUid();
        String useremail= email.getText().toString().trim();
        String apellidos = lastname.getText().toString().trim();
        String telefono = phone.getText().toString().trim();

        writeUser(use_id,username,apellidos,useremail,telefono);
}

    private String usernameFromEmail(String email)
    {
        if (email.contains("@")) {
            return email.split("@")[0];
        } else {
            return email;
        }
    }
    public   void writeUser(String userid, String name,String lastname ,String email,String phone)
    {
        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/" + userid, new User(
                name,
                lastname,
                email,
                phone
        ));

        mFirebaseDatabase.updateChildren(childUpdates);
    }

    @Override
    public void onItemClick() {

    }
}
