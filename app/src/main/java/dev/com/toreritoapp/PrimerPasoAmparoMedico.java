package dev.com.toreritoapp;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import dev.com.toreritoapp.util.DataManager;

public class PrimerPasoAmparoMedico extends Fragment
{
    public static final String ARG_OBJECT = "object";
    public  static  final String TAG = PrimerPasoAmparoMedico.class.getSimpleName();
    private Switch amparomenoredad;
    private TextInputLayout nombremenoredad;
    private TextInputLayout direccionotificacion;
    private TextInputLayout nombreamparo;
    private static  TextInputEditText   nombrecompletoamparo;
    private static  TextInputEditText  direccionotificar;
    private TextInputEditText menoredadnombre;
    private static  String personamparar;
    private static  String direccionamparo;
    private String nombredelmenor;
    private static DataManager dataManager;
    private SendData sendData;
    private Boolean esmenor;

    public static PrimerPasoAmparoMedico newInstance(int pos)
    {
        PrimerPasoAmparoMedico primerPasoAmparoMedico = new PrimerPasoAmparoMedico();
        Bundle args = new Bundle();
        args.putInt(PrimerPasoAmparoMedico.ARG_OBJECT, pos);
        primerPasoAmparoMedico.setArguments(args);
        return primerPasoAmparoMedico;
    }
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View  view =inflater.inflate(R.layout.primerpasoamparomedico, container, false);
        amparomenoredad = view.findViewById(R.id.switchmenoredad);
        nombremenoredad = view.findViewById(R.id.textinputnombremenoredad);
        nombreamparo =  view.findViewById(R.id.textinputnombreamparo);
        nombrecompletoamparo= view.findViewById(R.id.edt_nombrecompleto);
        direccionotificacion =view.findViewById(R.id.textinputdireccion);
        direccionotificar   = view.findViewById(R.id.edt_direccionnotificacion);
        menoredadnombre     = view.findViewById(R.id.edt_nombremenoredad);
        amparomenoredad.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {

                    nombremenoredad.setVisibility(View.VISIBLE);
                    nombreamparo.setHint("Nombre completo del padre o tutor");
                }
                else
                {
                    nombremenoredad.setVisibility(View.INVISIBLE);
                    nombreamparo.setHint("Nombre completo");


                }
            }
        });

        return view;
    }

    @Override
    public void onAttach(Context context) {
        try {
            sendData = (SendData) getActivity();
        }
        catch (ClassCastException e) {
            throw new ClassCastException("Error in retrieving data. Please try again");
        }

        super.onAttach(context);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        getData();
    }


    interface SendData {
        void sendData(String nombre, String direcccionnotificacion);
    }

    public void getData()
    {
         esmenor  = amparomenoredad.isChecked();
        if(esmenor)
        {
            personamparar = Objects.requireNonNull(nombrecompletoamparo.getText()).toString();
            direccionamparo = Objects.requireNonNull(direccionotificar.getText()).toString().trim();
            nombredelmenor = Objects.requireNonNull(menoredadnombre.getText().toString().trim());
            ((AmparoMedicoProceso) Objects.requireNonNull(getActivity())).nombre = personamparar;
            ((AmparoMedicoProceso) Objects.requireNonNull(getActivity())).direccionnotificacion = direccionamparo;
            ((AmparoMedicoProceso) Objects.requireNonNull(getActivity())).nombremenor = nombredelmenor;
            ((AmparoMedicoProceso) Objects.requireNonNull(getActivity())).esmenor = esmenor;
        }
        else
        {
            personamparar = Objects.requireNonNull(nombrecompletoamparo.getText()).toString();
            direccionamparo = Objects.requireNonNull(direccionotificar.getText()).toString().trim();

            ((AmparoMedicoProceso) Objects.requireNonNull(getActivity())).nombre = personamparar;
            ((AmparoMedicoProceso) Objects.requireNonNull(getActivity())).direccionnotificacion = direccionamparo;
            ((AmparoMedicoProceso) Objects.requireNonNull(getActivity())).esmenor = esmenor;

            String datos = String.format("Nombre :%s \n Direccion : %s ",personamparar,direccionamparo);
            Log.d(TAG, datos);

        }

    }
}
