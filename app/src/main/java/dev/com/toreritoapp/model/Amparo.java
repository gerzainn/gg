package dev.com.toreritoapp.model;

public class Amparo
{
    String fecha;
    String estado ;
    int tipo;


    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public String getEstatus() {
        return estatus;
    }

    public void setEstatus(String estatus) {
        this.estatus = estatus;
    }

    String estatus;

    public Amparo( String fecha, String estado, int tipo, String estatus) {
        this.fecha = fecha;
        this.estado = estado;
        this.tipo = tipo;
        this.estatus = estatus;
    }


}
