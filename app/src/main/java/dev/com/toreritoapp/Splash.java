package dev.com.toreritoapp;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class Splash extends AppCompatActivity
{
    private  int splashTime = 4000;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.splash);
        ImageView isologo = findViewById(R.id.iv_splash);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent i = new Intent(Splash.this,Login.class);
                startActivity(i);
                finish();
            }
        },splashTime);

        Animation myanim = AnimationUtils.loadAnimation(this,R.anim.bounce);
        isologo.startAnimation(myanim);

    }
}
