package dev.com.toreritoapp;

import android.os.Bundle;

import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

public class AmparoAlcholimetroProcesoAmigo extends FragmentActivity {

    private ViewPager viewPageralcholimetro;
    private TabLayout tabLayoutalcholimetro;
    public String googleid;
    public String nombre;
    public String correo;
    public String numerotelefono;
    public String nombreamigo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.amparo_alcholimetro_amigo_proceso);

        viewPageralcholimetro = findViewById(R.id.pageralcholimetroamigo);
        tabLayoutalcholimetro = findViewById(R.id.tabsamigoalchol);
        cargarViewPager(viewPageralcholimetro);
        tabLayoutalcholimetro.setupWithViewPager(viewPageralcholimetro);

    }

    public void cargarViewPager(ViewPager viewPager) {
        AdaptadorProceso adaptadorProceso = new AdaptadorProceso(getSupportFragmentManager());
        adaptadorProceso.addFragmnet(PrimerPasoAmparoAlcholimetroAmigo.newInstance(0));
        adaptadorProceso.addFragmnet(AmparoAlcholimetroPagoAmigo.newInstance(1));
        viewPager.setAdapter(adaptadorProceso);
    }


    public static class AdaptadorProceso extends FragmentStatePagerAdapter {

        private List<Fragment> fragments = new ArrayList<>();

        public AdaptadorProceso(FragmentManager fm) {
            super(fm);
        }

        private void addFragmnet(Fragment fragment) {
            fragments.add(fragment);

        }

        @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return "Paso " + (position + 1);
        }

        @Override
        public int getCount() {
            return fragments.size();
        }
    }
}