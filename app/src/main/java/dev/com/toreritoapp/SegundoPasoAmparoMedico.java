package dev.com.toreritoapp;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.fragment.app.Fragment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Objects;

import dev.com.toreritoapp.model.Estado;
import dev.com.toreritoapp.model.Municipio;
import dev.com.toreritoapp.util.DataManager;

import static android.content.Context.MODE_PRIVATE;

public class SegundoPasoAmparoMedico extends Fragment
{
    public  static  final String TAG = SegundoPasoAmparoMedico.class.getSimpleName();
    public static final String ARG_OBJECT = "object";
    private AppCompatSpinner spinerEstado;
    private AppCompatSpinner spinermunicipio;
    private ArrayList<Estado> estados;
    private ArrayList<Municipio> municipios;
    private  DataManager dataManager;
    private Button pasosiguiente;
    private ArrayAdapter<Estado> estadoArrayAdapter;
    private ArrayAdapter<Municipio> municipioArrayAdapter;
    private String nombreinstituomedico;
    private String direccioninstituomedico;
    private String estado;
    private TextInputEditText editTextnombreinstituto;
    private  TextInputEditText editTextdireccioninstituto;


    public static SegundoPasoAmparoMedico newInstance(int pos)
    {
        SegundoPasoAmparoMedico segundoPasoAmparoMedico = new SegundoPasoAmparoMedico();
        Bundle args = new Bundle();
        args.putInt(SegundoPasoAmparoMedico.ARG_OBJECT, pos);
        segundoPasoAmparoMedico.setArguments(args);
        return segundoPasoAmparoMedico;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
            View  view =inflater.inflate(R.layout.segundopasoamparomedico, container, false);
            spinerEstado = view.findViewById(R.id.spinnerestado);
            spinermunicipio= view.findViewById(R.id.spinnermunicipio);
            editTextnombreinstituto = view.findViewById(R.id.edt_nombreinstituomedico);
            editTextdireccioninstituto= view.findViewById(R.id.edt_direccioninstituomedico);

            estados = new ArrayList<>();
            municipios = new ArrayList<>();



            Estado hidalgo = new Estado(1,"Hidalgo");
            Estado morelos = new Estado(2, "Morelos");
            Estado estadoMexico = new Estado(3, "Estado de Mexico");
            Estado jalisco      = new Estado(4, "Jalisco");
            Estado nuevoleon    = new Estado(5, "Nuevo Leon");
            Estado sonora       = new Estado(6, "Sonora");
            Estado puebla       = new Estado(7,"Puebla");
            Estado veracruz     = new Estado(8,"Veracruz");
            Estado coahuila     = new Estado(9, "Coahuila de Zaragoza");
            Estado sanluispotosi = new Estado(10, "San Luis Potosi");
            Estado tabasco       = new Estado(11, "Tabasco");
            Estado michoacan     = new Estado(12, "Michoacan");
            Estado sinaloa       = new Estado(13, "Sinaloa");
            Estado oaxaca        = new Estado(14, "Oaxaca");
            Estado yucatan       = new Estado(15, "Yucatan");
            Estado bajacalifornia = new Estado(16, "Baja California");
            Estado guanajuato     = new Estado(17, "Guanajuato");
            Estado chihuahua      = new Estado(18, "Chihuahua");
            Estado tamaulipas     = new Estado(19, "Tamaulipas");
            Estado chiapas        = new Estado( 20, "Chiapas");
            Estado guerrero       = new Estado( 21, "Guerrero");
            Estado queretaro      = new Estado(22, "Queretaro");
            Estado zacatecas      = new Estado(24, "Zacatecas");
            Estado nayarit        = new Estado(25, "Nayarit");
            Estado bajacaliforniasur = new Estado( 26, "Baja California Sur");
            Estado durango        = new Estado(27, "Durango");
            Estado quintanaroo     = new Estado( 28, "Quintana roo");
            Estado tlaxcala        = new Estado(29, "Tlaxcala");
            Estado aguascalientes  = new Estado(30, "AguasCalientes");
            Estado campeche        = new Estado( 31, "Campeche");
            Estado colima          = new Estado(32, "Colima");
            estados.add(hidalgo);
            estados.add(morelos);
            estados.add(estadoMexico);
            estados.add(jalisco);
            estados.add(nuevoleon);
            estados.add(sonora);
            estados.add(puebla);
            estados.add(coahuila);
            estados.add(sanluispotosi);
            estados.add(tabasco);
            estados.add(michoacan);
            estados.add(sinaloa);
            estados.add(oaxaca);
            estados.add(yucatan);
            estados.add(bajacalifornia);
            estados.add(guanajuato);
            estados.add(chihuahua);
            estados.add(tamaulipas);
            estados.add(chiapas);
            estados.add(guerrero);
            estados.add(queretaro);
            estados.add(zacatecas);
            estados.add(nayarit);
            estados.add(bajacaliforniasur);
            estados.add(durango);
            estados.add(quintanaroo);
            estados.add(tlaxcala);
            estados.add(aguascalientes);
            estados.add(campeche);
            estados.add(colima);

        Collections.sort(estados, new Comparator<Estado>() {
            @Override
            public int compare(Estado o1, Estado o2) {
                return o1.getNombre().compareToIgnoreCase(o2.getNombre());
            }
        });

            Municipio cuernavaca = new Municipio(0,morelos,"Cuernavaca");
            Municipio municipioPachuca  = new Municipio(1, hidalgo,"Pachuca de Soto");
            Municipio Toluca   = new Municipio(2,estadoMexico,"Toluca");
            Municipio NAUCALPAN         = new Municipio(3,estadoMexico,"Naucalpan de Juárez");
            Municipio nezahualcoyotl  = new Municipio(4, estadoMexico,"Nezahualcóyotl");
            Municipio guadalajara     = new Municipio(5, jalisco,"Guadalajara");
            Municipio monterrey       = new Municipio(6,nuevoleon,"Monterrey");
            Municipio hermosillo      = new Municipio(7, sonora,"Hermosillo");
            Municipio nogales         = new Municipio(8, sonora,"Nogales");
            Municipio ciudadobregon   = new Municipio(9,sonora,"Ciudad Obregón");
            Municipio auguaprieta     = new Municipio(10,sonora,"Agua Prieta");
            Municipio xalapa          = new Municipio(11,veracruz,"Xalapa");
            Municipio bocadelrio      = new Municipio(12, veracruz,"Boca del rio");
            Municipio cordoba         = new Municipio(13,veracruz,"Córdoba");
            Municipio tuxpan          = new Municipio(14,veracruz,"Tuxpan");
            Municipio pozarica        = new Municipio(15,veracruz,"Poza Rica");
            Municipio coatzacoalcos   = new Municipio(16 ,veracruz,"Coatzacoalcos");
            Municipio saltillo        = new Municipio(16,coahuila,"Saltillo");
            Municipio piedrasnegras   = new Municipio(17, coahuila,"Piedras Negras");
            Municipio monclova        = new Municipio(18,coahuila,",Monclova");
            Municipio torreon         = new Municipio(19, coahuila,"Torreon");
            Municipio sanluis         = new Municipio(20,sanluispotosi, "SanLuisPotosi");
            Municipio ciudadvalles    = new Municipio(21, sanluispotosi,"Ciudad Valles");
            Municipio villahermosa    = new Municipio(22, tabasco, "Villa Hermosa");
            Municipio morelia         = new Municipio(23, michoacan, "Morelia");
            Municipio uruapan         = new Municipio(24, michoacan, "Uruapan");
            Municipio culiacan        = new Municipio(25, sinaloa,"Culiacán");
            Municipio mochis          = new Municipio(26, sinaloa ,"Mochis");
            Municipio mazatlan        = new Municipio(27, sinaloa , "Mazatlán");
            Municipio oaxacacapital   = new Municipio(28, oaxaca ,"Oaxaca");
            Municipio salinacruz      = new Municipio(29,oaxaca,"Salina Cruz");
            Municipio merida          = new Municipio(30,yucatan,"Mérida");
            Municipio mexicali        = new Municipio(31, bajacalifornia, "Mexicali");
            Municipio tijuana         = new Municipio(32, bajacalifornia, "Tijuana");
            Municipio ensenada        = new Municipio(33, bajacalifornia, "Ensenada");
            Municipio guanajutoc      = new Municipio(34, guanajuato, "Guanajuato");
            Municipio leon            = new Municipio(35, guanajuato ,"León");
            Municipio celaya          = new Municipio( 36, guanajuato,"Celaya");
            Municipio chihuahuac      = new Municipio(37,chihuahua,"Chihuahua");
            Municipio ciudadjuarez    = new Municipio(38,chihuahua,"Ciudad Juárez");
            Municipio ciudadvitoria   = new Municipio( 39,tamaulipas, "Ciudad Victoria");
            Municipio nuevolaredo     = new Municipio(40, tamaulipas,"Nuevo Laredo");
            Municipio matamoros       = new Municipio(41,tamaulipas, "Matamoros");
            Municipio reynosa         = new Municipio(42, tamaulipas,"Reynosa");
            Municipio tampico         = new Municipio(43, tamaulipas, "Tampico");
            Municipio tuxtla          = new Municipio(44, chiapas,"Tuxtla Gutiérrez");
            Municipio tapachula       = new Municipio(45, chiapas,"Tapachula");
            Municipio chipalcingo     = new Municipio(46, guerrero ,"Chilpancingo");
            Municipio acapulco        = new Municipio(47, guerrero, "Acapulco");
            Municipio iguala          = new Municipio(48, guerrero,"Iguala");
            Municipio  tepic          = new Municipio(49, nayarit, "Tepic");
            Municipio lapaz           = new Municipio( 50, bajacaliforniasur, "La Paz");
            Municipio durangoc         = new Municipio(60, durango ,"Durango");
            Municipio chetumal        = new Municipio(61, quintanaroo,"Chetumal");
            Municipio cancun          = new Municipio(62,quintanaroo,"Cancún");
            Municipio tlaxcalac        = new Municipio(63, tlaxcala,"Tlaxcala");
            Municipio aguascalientesc  = new Municipio(64, aguascalientes,"Aguascalientes");
            Municipio campechec        = new Municipio(65,campeche,"Campeche");
            Municipio queretaroc       = new Municipio(66, queretaro,"Queretaro");
            municipios.add(cuernavaca);
            municipios.add(municipioPachuca);
            municipios.add(Toluca);
            municipios.add(NAUCALPAN);
            municipios.add(nezahualcoyotl);
            municipios.add(guadalajara);
            municipios.add(monterrey);
            municipios.add(hermosillo);
            municipios.add(nogales);
            municipios.add(ciudadobregon);
            municipios.add(auguaprieta);
            municipios.add(xalapa);
            municipios.add(bocadelrio);
            municipios.add(coatzacoalcos);
            municipios.add(cordoba);
            municipios.add(tuxpan);
            municipios.add(pozarica);
            municipios.add(saltillo);
            municipios.add(piedrasnegras);
            municipios.add(monclova);
            municipios.add(torreon);
            municipios.add(ciudadvalles);
            municipios.add(sanluis);
            municipios.add(villahermosa);
            municipios.add(morelia);
            municipios.add(uruapan);
            municipios.add(culiacan);
            municipios.add(mochis);
            municipios.add(mazatlan);
            municipios.add(oaxacacapital);
            municipios.add(salinacruz);
            municipios.add(merida);
            municipios.add(mexicali);
            municipios.add(tijuana);
            municipios.add(ensenada);
            municipios.add(guanajutoc);
            municipios.add(leon);
            municipios.add(celaya);
            municipios.add(chihuahuac);
            municipios.add(ciudadjuarez);
            municipios.add(ciudadvitoria);
            municipios.add(nuevolaredo);
            municipios.add(matamoros);
            municipios.add(reynosa);
            municipios.add(tampico);
            municipios.add(tuxtla);
            municipios.add(tapachula);
            municipios.add(chipalcingo);
            municipios.add(acapulco);
            municipios.add(iguala);
            municipios.add(tepic);
            municipios.add(lapaz);
            municipios.add(durangoc);
            municipios.add(chetumal);
            municipios.add(cancun);
            municipios.add(tlaxcalac);
            municipios.add(aguascalientesc);
            municipios.add(campechec);
            municipios.add(queretaroc);

            Collections.sort(municipios, new Comparator<Municipio>() {
                @Override
                public int compare(Municipio o1, Municipio o2) {
                    return o1.getNombre().compareToIgnoreCase(o2.getNombre());
                }
            });
            estadoArrayAdapter = new ArrayAdapter<Estado>(getActivity().getApplicationContext(),R.layout.support_simple_spinner_dropdown_item,estados);
            estadoArrayAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
            spinerEstado.setAdapter(estadoArrayAdapter);

            municipioArrayAdapter= new ArrayAdapter<Municipio>(getActivity().getApplicationContext(),R.layout.support_simple_spinner_dropdown_item,municipios);
            municipioArrayAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
            spinermunicipio.setAdapter(municipioArrayAdapter);

            spinerEstado.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                        final Estado estado= (Estado) spinerEstado.getItemAtPosition(i);
                        Log.d("SpinerEstado","Seleciono"+ estado.getIdEstado());

                        ArrayList<Municipio> tempmunicipo = new ArrayList<>();

                        for (Municipio municipio : municipios)
                        {
                            if(municipio.getEstado().getIdEstado() == estado.getIdEstado())
                            {
                                tempmunicipo.add(municipio);
                            }
                        }

                        municipioArrayAdapter = new ArrayAdapter<>(getContext(),R.layout.support_simple_spinner_dropdown_item,tempmunicipo);
                        municipioArrayAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
                        spinermunicipio.setAdapter(municipioArrayAdapter);
                    }



                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            return  view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

    }


    @Override
    public void onPause() {
        super.onPause();
        obtenerDatos();
    }

    @Override
    public void onStop() {
        super.onStop();
        obtenerDatos();
    }

    public void getData(String nombre , String direccioninstituomedico)
    {

    }
    public  void obtenerDatos()
    {
        nombreinstituomedico = editTextnombreinstituto.getText().toString();
        direccioninstituomedico = editTextdireccioninstituto.getText().toString();
        estado = spinerEstado.getSelectedItem().toString();

        ((AmparoMedicoProceso) Objects.requireNonNull(getActivity())).institutomedico = nombreinstituomedico;

        ((AmparoMedicoProceso) Objects.requireNonNull(getActivity())).direccioninstitutomedico = direccioninstituomedico;

        ((AmparoMedicoProceso) Objects.requireNonNull(getActivity())).estado = estado;

        String datos = String.format("Instituto :%s \n Direccion : %s  \n Estado :%s",nombreinstituomedico,direccioninstituomedico, estado);
        Log.d(TAG, datos);
    }
}


