package dev.com.toreritoapp.model;

import com.squareup.moshi.Json;

public class Estado implements  Comparable<Estado> {

    @Json(name = ("id"))
    private int idEstado;
    @Json(name = ("nombre"))
    private String nombre;

    public Estado(int idEstado, String nombre)
    {
        this.idEstado = idEstado;
        this.nombre = nombre;

    }

    public int getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(int idEstado) {
        this.idEstado = idEstado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }


    @Override
    public int compareTo(Estado estado)
    {
        return this.getIdEstado() - estado.getIdEstado();
    }


    @Override
    public String toString() {
        return nombre;
    }
}
