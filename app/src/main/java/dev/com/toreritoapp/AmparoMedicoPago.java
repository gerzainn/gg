package dev.com.toreritoapp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.BillingResult;
import com.android.billingclient.api.ConsumeParams;
import com.android.billingclient.api.ConsumeResponseListener;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;
import com.android.billingclient.api.SkuDetailsResponseListener;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import dev.com.toreritoapp.model.AmparoMedico;
import dev.com.toreritoapp.util.TipoLogin;



public class AmparoMedicoPago extends Fragment implements PurchasesUpdatedListener {
    public static final String ARG_OBJECT = "object";
    private BillingClient mBillingClient;
    private  final static  String Tag = AmparoMedicoPago.class.getSimpleName();
    private  int codigorespuesta;
    private  int tamlista;
    private  String orderId;
    private String googelid;
    private String firebaseId;
    private  String nombrepersona;
    private  String direccion;
    private String nombreinstituomedico;
    private String direccioninstituomedico;
    private String estado;
    private String municipio;
    private String fechaamparo;
    private String padecimiento;
    private TextView nombreproducto;
    private TextView precioproducto;
    private TipoLogin tipoLogin;
    private String nombremenor;
    private Boolean esmenor;
    private DatabaseReference mFirebaseDatabase;
    private FirebaseDatabase mFirebaseInstance;
    private FirebaseAuth mauth;
    private ConstraintLayout contenedor;

    public static AmparoMedicoPago newInstance(int pos,String googelid)
    {
        AmparoMedicoPago amparoMedicoPago = new AmparoMedicoPago();
        Bundle args = new Bundle();
        args.putInt(AmparoMedicoPago.ARG_OBJECT, pos);
        args.putString("googleid",googelid);
        amparoMedicoPago.setArguments(args);

        return  amparoMedicoPago;
    }

    @Override
    public void onAttach(Context context) {

        nombrepersona = ((AmparoMedicoProceso) Objects.requireNonNull(getActivity())).nombre;
        direccion     = ((AmparoMedicoProceso)getActivity()).direccionnotificacion;

        super.onAttach(context);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        nombrepersona = ((AmparoMedicoProceso) Objects.requireNonNull(getActivity())).nombre;
        direccion     = ((AmparoMedicoProceso)getActivity()).direccionnotificacion;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }

    @Override
    public void onPause() {
        fechaamparo = ((((AmparoMedicoProceso) Objects.requireNonNull(getActivity())).fechaamparo));
        padecimiento = ((((AmparoMedicoProceso)getActivity()).padecimiento));
        nombreinstituomedico = ((AmparoMedicoProceso)getActivity()).institutomedico;
        direccioninstituomedico =  ((AmparoMedicoProceso)getActivity()).direccioninstitutomedico;
        estado = ((AmparoMedicoProceso)getActivity()).estado;
        municipio = ((AmparoMedicoProceso)getActivity()).municipio;
        esmenor = ((AmparoMedicoProceso)getActivity()).esmenor;
        nombremenor = ((AmparoMedicoProceso)getActivity()).nombremenor;
        super.onPause();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.amparomedicopago, container, false);
        Button pago = view.findViewById(R.id.pagoamparoamedico);
        nombreproducto= view.findViewById(R.id.tv_productoamparomedico);
        precioproducto = view.findViewById(R.id.tv_precioamparomedico);
        contenedor     = view.findViewById(R.id.contenedor_amparomedico);
        mFirebaseInstance = FirebaseDatabase.getInstance();
        mFirebaseDatabase = mFirebaseInstance.getReference("users");
        Bundle parametros = getArguments();
        if(parametros!= null)
        {
            googelid= parametros.getString("googleid");

        }
        pago.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProcesarPago();
            }
        });
        setUpBillig();

        mauth = FirebaseAuth.getInstance();
        return  view;
    }

    public void setUpBillig()
    {
        mBillingClient = BillingClient
                .newBuilder(Objects.requireNonNull(getContext()))
                .setListener(this)
                .enablePendingPurchases()
                .build();
        mBillingClient.startConnection(new BillingClientStateListener()
        {

            @Override
            public void onBillingSetupFinished(BillingResult billingResult) {
                if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK)
                {
                    Log.d(Tag, "RespuestaOK");
                    GetAmparo();
                }
                else
                {
                    Log.e(Tag , "error" + billingResult.getResponseCode());
                }
            }

            @Override
            public void onBillingServiceDisconnected() {

                Log.e(Tag ,"Desconectado");
            }
        });
    }

    public void GetAmparo()
    {
        List<String> amparo = new ArrayList<>();
        amparo.add("amparosalud_1");

        SkuDetailsParams.Builder params = SkuDetailsParams
                .newBuilder();
        params.setSkusList(amparo)
                .setType(BillingClient.SkuType.INAPP)
                .build();
        mBillingClient.querySkuDetailsAsync(params.build(), new SkuDetailsResponseListener() {
            @Override
            public void onSkuDetailsResponse(BillingResult billingResult, List<SkuDetails> list) {

                if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK  )
                {
                    codigorespuesta = billingResult.getResponseCode();
                    tamlista= list.size();
                    Log.d(Tag, "Sucees " +codigorespuesta +" "+ "Tam"+ tamlista);

                    for (SkuDetails skuDetails : list)
                    {
                        String sku = skuDetails.getDescription();
                        String price = skuDetails.getPrice();
                        nombreproducto.setText(sku);
                        precioproducto.setText(price);

                        Log.d(Tag , "precio" + price + " sku"+ " " + sku);
                    }
                }

                else
                {
                    Log.e(Tag,  String.valueOf(codigorespuesta));
                }
            }
        });

    }

    public void ProcesarPago()
    {
        List<String> products = new ArrayList<>();
        products.add("amparosalud_1");

        if(mBillingClient.isReady())
        {
            SkuDetailsParams.Builder params = SkuDetailsParams
                    .newBuilder();
            params.setSkusList(products)
                    .setType(BillingClient.SkuType.INAPP)
                    .build();
            mBillingClient.querySkuDetailsAsync(params.build(), new SkuDetailsResponseListener() {
                @Override
                public void onSkuDetailsResponse(BillingResult billingResult, List<SkuDetails> list) {

                    if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK  )
                    {
                        codigorespuesta = billingResult.getResponseCode();
                        tamlista= list.size();
                        Log.d(Tag, "Sucees " +codigorespuesta +" "+ "Tam"+ tamlista);



                        for (SkuDetails skuDetails : list)
                        {
                            String sku = skuDetails.getSku();
                            String price = skuDetails.getPrice();

                            BillingFlowParams flowParams = BillingFlowParams.newBuilder()
                                    .setSkuDetails(skuDetails)
                                    .build();
                            BillingResult responsecode = mBillingClient.launchBillingFlow(getActivity(),flowParams);
                            int response = responsecode.getResponseCode();

                            Log.d(Tag , "precio" + price + " sku"+ " " + sku);
                            Log.d(Tag, "codigoBilligResult"+ " " + response);
                        }

                    }

                    else
                    {
                        Log.e(Tag,  String.valueOf(codigorespuesta));
                    }
                }
            });

        }
        else
        {
            Toast.makeText(getContext(),"BillingNotReady",Toast.LENGTH_LONG).show();
        }
    }

    public void  handlePurchase(Purchase purchase)
    {
        if(purchase!=null)
        {
            ConsumeParams consumeParams= ConsumeParams.newBuilder()
                    .setPurchaseToken(purchase.getPurchaseToken())
                    .build();

            ConsumeResponseListener consumeResponseListener= new ConsumeResponseListener() {
                @Override
                public void onConsumeResponse(BillingResult billingResult, String s) {
                    if(billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK && s!= null)
                    {
                        Log.d(Tag, "onConsumeResponseOk");

                    }
                }
            };
            mBillingClient.consumeAsync(consumeParams,consumeResponseListener);
        }
    }

    public void styleSnackbar(View view) {
        Snackbar snackbar = Snackbar.make(view, getString(R.string.errorcompra), Snackbar.LENGTH_LONG)
                .setAction("", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                    }
                });

        //Accion
        snackbar.setActionTextColor(getResources().getColor(R.color.whiteTextColor));
        View snackBarView = snackbar.getView();
        //Fondo
        snackBarView.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        //Imagen
        snackbar.show();
    }

    @Override
    public void onPurchasesUpdated(BillingResult billingResult, @Nullable List<Purchase> list) {
        if(billingResult.getResponseCode() ==  BillingClient.BillingResponseCode.OK
                && list != null)
        {
            for (Purchase purchase : list)
            {
                handlePurchase(purchase);
                orderId= purchase.getOrderId();

            } // Termina  loop

            crearAmparomedico();

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent i = new Intent(getActivity(),CompraCompletada.class);
                    startActivity(i);

                }
            },3000L);

        }
        else if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.USER_CANCELED)
        {
            Toast.makeText(getContext(), "Se cancelo la compra", Toast.LENGTH_SHORT).show();
        }
        else if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.ITEM_ALREADY_OWNED)
        {
            Toast.makeText(getContext(), "El producto ya se compro anteriormente ", Toast.LENGTH_SHORT).show();

        }
        else if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.ERROR)
        {
            styleSnackbar(contenedor);
        }
    }

    public  void crearAmparomedico()
    {
        mFirebaseInstance = FirebaseDatabase.getInstance();
        SimpleDateFormat format = new SimpleDateFormat("dd-MMM--yyyy", Locale.getDefault());
        String datestring = format.format(new Date());


        if(mauth.getCurrentUser()!= null)
        {
            FirebaseUser firebaseUser = mauth.getCurrentUser();
            assert firebaseUser != null;
            firebaseId = firebaseUser.getUid();

            mFirebaseDatabase = mFirebaseInstance.getReference("users")
                    .child(firebaseId)
                    .child("amparosmedicos");

            if(esmenor)
            {
                orderId= orderId.replaceAll("[^A-Za-z0-9]", "");
                Map<String,Object> amparoMap = new HashMap<>();
                amparoMap.put("/"+orderId, new AmparoMedico(
                        nombrepersona,
                        direccion,
                        nombremenor,
                        nombreinstituomedico,
                        direccioninstituomedico,
                        estado,
                        municipio,
                        fechaamparo,
                        padecimiento,
                        3,
                        esmenor,
                        "Pendiente"));
                mFirebaseDatabase.updateChildren(amparoMap);

                mFirebaseDatabase = mFirebaseInstance.getReference("sinprocesarmedicos").push();
                Map<String ,Object> procesaramparo = new HashMap<>();
                String rutactualizar= "/users/"+firebaseId+"/amparosmedicos/"+orderId;
                procesaramparo.put("ruta",rutactualizar);
                mFirebaseDatabase.updateChildren(procesaramparo);
            }
            else
            {
                orderId= orderId.replaceAll("[^A-Za-z0-9]", "");
                Map<String, Object> userMap = new HashMap<>();
                userMap.put("/"+orderId, new AmparoMedico(
                        nombrepersona,
                        direccion,
                        nombreinstituomedico,
                        direccioninstituomedico,
                        estado,
                        municipio,
                        fechaamparo,
                        padecimiento,
                        1,
                        false,
                        "Pendiente"

                ));
                mFirebaseDatabase.updateChildren(userMap);
                mFirebaseDatabase = mFirebaseInstance.getReference("sinprocesarmedicos").push();
                Map<String ,Object> procesaramparo = new HashMap<>();
                String rutactualizar= "/users/"+firebaseId+"/amparosmedicos/"+orderId;
                procesaramparo.put("ruta",rutactualizar);
                mFirebaseDatabase.updateChildren(procesaramparo);
            }

        }
        else {


            GoogleSignInAccount acct = GoogleSignIn.getLastSignedInAccount(Objects.requireNonNull(getActivity()));
            if (acct != null) {
                 googelid = acct.getId();

                mFirebaseDatabase = mFirebaseInstance.getReference("users")
                        .child(googelid)
                        .child("amparosmedicos");
                if(esmenor)
                {
                    orderId= orderId.replaceAll("[^A-Za-z0-9]", "");
                    Map<String,Object> amparoMap = new HashMap<>();
                    amparoMap.put("/"+orderId, new AmparoMedico(
                            nombrepersona,
                            direccion,
                            nombremenor,
                            nombreinstituomedico,
                            direccioninstituomedico,
                            estado,
                            municipio,
                            fechaamparo,
                            padecimiento,
                            3,
                            esmenor,
                            "Pendiente"));

                    mFirebaseDatabase.updateChildren(amparoMap);
                    mFirebaseDatabase = mFirebaseInstance.getReference("sinprocesarmedicos").push();
                    Map<String ,Object> procesaramparo = new HashMap<>();
                    String rutactualizar= "/users/"+googelid+"/amparosmedicos/"+orderId;
                    procesaramparo.put("ruta",rutactualizar);
                    mFirebaseDatabase.updateChildren(procesaramparo);
                }
                else
                {
                    orderId= orderId.replaceAll("[^A-Za-z0-9]", "");
                    Map<String, Object> userMap = new HashMap<>();
                    userMap.put("/"+orderId, new AmparoMedico(
                            nombrepersona,
                            direccion,
                            nombreinstituomedico,
                            direccioninstituomedico,
                            estado,
                            municipio,
                            fechaamparo,
                            padecimiento,
                            1,
                            esmenor,
                            "Pendiente"));
                    mFirebaseDatabase.updateChildren(userMap);

                    mFirebaseDatabase = mFirebaseInstance.getReference("sinprocesarmedicos").push();
                    Map<String ,Object> procesaramparo = new HashMap<>();
                    String rutactualizar= "/users/"+googelid+"/amparosmedicos/"+orderId;
                    procesaramparo.put("ruta",rutactualizar);
                    mFirebaseDatabase.updateChildren(procesaramparo);
                }
            }
        }

    }
}
