package dev.com.toreritoapp;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import java.util.ArrayList;
import java.util.List;


public class AmparoMedicoProceso extends FragmentActivity   implements PrimerPasoAmparoMedico.SendData {

    private ViewPager viewPager;
    private AdaptadorPasos adapter;
    private String googleid;
    public String firebaseId;
    public String nombre ;
    public String nombremenor;
    public String direccionnotificacion;
    public String institutomedico;
    public String direccioninstitutomedico;
    public String estado;
    public String municipio;
    public String fechaamparo;
    public String padecimiento;
    public Boolean esmenor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_amparo_medico_proceso);
        Bundle parametros = getIntent().getExtras();
        if(parametros!= null)
        {
            googleid= parametros.getString("googleid");

        }
        TabLayout tabLayout = findViewById(R.id.tabs);
        viewPager = findViewById(R.id.pager);
        cargarViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {


            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


    }

    public void cargarViewPager(ViewPager viewPager)
    {
        AdaptadorPasos adapter = new AdaptadorPasos(getSupportFragmentManager());
        adapter.addFragmnet(PrimerPasoAmparoMedico.newInstance(0));
        adapter.addFragmnet(SegundoPasoAmparoMedico.newInstance(1));
        adapter.addFragmnet(PasoTresAmparoMedico.newInstance(2));
        adapter.addFragmnet(AmparoMedicoPago.newInstance(3,googleid));
        viewPager.setAdapter(adapter);
    }


    @Override
    public void sendData(String nombre, String direcccionnotificacion) {
        String tag = "android:switcher:" + R.id.pager + ":" + viewPager.getCurrentItem();
        Fragment f =  getSupportFragmentManager().findFragmentByTag(tag);
        assert f != null;
    }

    public class AdaptadorPasos extends FragmentStatePagerAdapter
    {

        private  List<Fragment> fragments = new ArrayList<>();
        private AdaptadorPasos(FragmentManager fragmentManager)
        {
            super(fragmentManager);
        }

        private  void  addFragmnet(Fragment fragment)
        {
            fragments.add(fragment);

        }

        @Override
        public Fragment getItem(int position) {
            return  fragments.get(position);

        }
        public CharSequence getPageTitle(int position) {
            return "Paso " + (position + 1);
        }

        @Override
        public int getCount() {
            return fragments.size();
        }
    }







}
