package dev.com.toreritoapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class AmparoAtencionMedica extends AppCompatActivity {
    private Button btnprocesaramparo;
    private String googleid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_amparo_atencion_medica);
        btnprocesaramparo= findViewById(R.id.btnprocesaramparo);
        Bundle parametros = getIntent().getExtras();
        if(parametros!= null)
        {
            googleid= parametros.getString("googleid");
        }
        btnprocesaramparo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), AmparoMedicoProceso.class);
                intent.putExtra("googleid",googleid);
                startActivity(intent);
            }
        });
    }
}
