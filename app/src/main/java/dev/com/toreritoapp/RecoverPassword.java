package dev.com.toreritoapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;


import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;

public class RecoverPassword extends AppCompatActivity implements  ActionBottomDialogUser.ItemClickListener {

    private TextInputLayout inputLayoutemail;
    private TextInputEditText email;
    private Button enviarpassword;
    private TextView error;
    private  Boolean iserror = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recover_password);
        inputLayoutemail = findViewById(R.id.contenedor_recuperarpass);
        email = findViewById(R.id.edtrecoverpass);
        enviarpassword = findViewById(R.id.btnrecuperarpass);
        error     = findViewById(R.id.tv_error_email_recover);

        email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (!Patterns.EMAIL_ADDRESS.matcher(charSequence).matches())
                {
                    inputLayoutemail.setError("El correo no esta en formato correcto");
                    email.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_warning,0);
                    enviarpassword.setBackgroundResource(R.drawable.btn_background_gray);
                    enviarpassword.setEnabled(false);
                }
                else
                {
                    inputLayoutemail.setError(null);
                    enviarpassword.setBackgroundResource(R.drawable.button_background_red);
                    email.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_check_ok,0);
                    enviarpassword.setEnabled(true);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        RecoverPassword.this.setFinishOnTouchOutside(false);


        enviarpassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!validar()) {
                    return;
                }
                FirebaseAuth auth = FirebaseAuth.getInstance();
                String emailAddress = email.getText().toString();

                auth.sendPasswordResetEmail(emailAddress)
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                               if(iserror)
                               {
                                   error.setVisibility(View.VISIBLE);
                               }
                               else
                               {
                                   iserror=false;
                                   error.setVisibility(View.GONE);
                               }

                            }
                        })
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    Log.d("EmailRecover", "Email sent.");
                                    EmailDialog();
                                }

                                else
                                {

                                }
                            }
                        });

            }
        });

    }


    public boolean validar() {
        boolean valid = true;
        String correo = email.getText().toString();
        if (TextUtils.isEmpty(correo)) {

            valid = false;
            inputLayoutemail.setError(getString(R.string.validaremail));
            enviarpassword.setBackgroundResource(R.drawable.btn_background_gray);
        } else {
            inputLayoutemail.setError(null);
            enviarpassword.setBackgroundResource(R.drawable.button_background_red);
        }
        return  valid;
    }
    public void EmailDialog()
    {
        ActionBottomDialogUser actionBottomDialogUser =
                ActionBottomDialogUser.newInstance();
        actionBottomDialogUser.show(getSupportFragmentManager(),
                ActionBottomDialogUser.TAG);
    }

    @Override
    public void onItemClick() {

    }
}
