package dev.com.toreritoapp;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import dev.com.toreritoapp.model.AmparoAlcholimetro;

public class AmparoAlcholimetroAdapter  extends FirebaseRecyclerAdapter<AmparoAlcholimetro,AmparoAlcholimetroAdapter.ViewHolderAlcholimetro>
{

    /**
     * Initialize a {@link RecyclerView.Adapter} that listens to a Firebase query. See
     * {@link FirebaseRecyclerOptions} for configuration options.
     *
     * @param options
     */
    public AmparoAlcholimetroAdapter(@NonNull FirebaseRecyclerOptions<AmparoAlcholimetro> options) {
        super(options);
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolderAlcholimetro holder, int position, @NonNull AmparoAlcholimetro model) {
        switch (model.getTipo())
        {
            case  2 :
                holder.direccion.setText("Personal");
                holder.fecha.setText(model.getFecha());
                holder.nombrepersona.setText(model.getNombre());

                break;
            case 3 :
                holder.direccion.setText("Amigo");
                holder.fecha.setText(model.getFecha());
                holder.nombreamigo.setVisibility(View.VISIBLE);
                holder.nombreamigo.setText(model.getNombreamigo());
                holder.nombrepersona.setText(model.getNombre());
                break;
        }
    }

    @NonNull
    @Override
    public ViewHolderAlcholimetro onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_amparo,parent,false);

        return  new ViewHolderAlcholimetro(view);
    }

    public class ViewHolderAlcholimetro extends RecyclerView.ViewHolder
    {
        public TextView nombrepersona;
        public ImageView amparo;
        public  TextView direccion;
        public TextView fecha;
        public TextView nombreamigo;
        public ViewHolderAlcholimetro(@NonNull View itemView)
        {
            super(itemView);
            nombrepersona=itemView.findViewById(R.id.tv_amparo);
            amparo= itemView.findViewById(R.id.iv_amparolista);
            nombreamigo = itemView.findViewById(R.id.tv_nombremenor);
            direccion=itemView.findViewById(R.id.tv_direccionotificacion);
            fecha=itemView.findViewById(R.id.tv_fechamparo);
        }
    }
}

