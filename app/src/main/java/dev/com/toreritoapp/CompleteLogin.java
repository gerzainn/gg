package dev.com.toreritoapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import dev.com.toreritoapp.model.User;

public class CompleteLogin extends AppCompatActivity implements  GoogleApiClient.OnConnectionFailedListener , ActionBottomDialogCompleteGoogleAccount.ItemClickListener {

    private DatabaseReference mFirebaseDatabase;
    private  DatabaseReference databaseReferencesave;
    private FirebaseDatabase mFirebaseInstance;
    private GoogleApiClient googleApiClient;
    private String      googleid;
    private TextInputEditText editemail,nombre,lastname, phone;
    private TextInputLayout inputLayoutnombre, inputLayoutapellido, inputLayoutphone ,inputLayoutemail;
    private Button  btnguardar;
    private ConstraintLayout constraintLayoutcarga, constraintLayoutprincipal;
    private int  animeTime = 2800;
    public static final String Second = "MyPrefs" ;
    SharedPreferences sharedpreferences;
    private static final String TAG = CompleteLogin.class.getSimpleName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.completelogingoogle);

        mFirebaseInstance = FirebaseDatabase.getInstance();
        databaseReferencesave= mFirebaseInstance.getReference("users");

        editemail               = findViewById(R.id.edt_emailgoogle);
        nombre                  = findViewById(R.id.edt_nombreinstituomedico);
        lastname                = findViewById(R.id.edt_apellidosgoogle);
        phone                   = findViewById(R.id.edt_telefonogoolge);
        inputLayoutnombre       = findViewById(R.id.textinputgooglename);
        inputLayoutapellido     = findViewById(R.id.textinputapellidosgoogle);
        inputLayoutphone        = findViewById(R.id.textInputLayouttelefonogoogle);
        inputLayoutemail        = findViewById(R.id.textinputemailgoogle);
        btnguardar              = findViewById(R.id.btnguardardatos);
        constraintLayoutcarga   = findViewById(R.id.contenedorcargarpantalla);
        constraintLayoutprincipal  = findViewById(R.id.vistaprinciapl);
        btnguardar.setBackgroundResource(R.drawable.btn_background_gray);

        sharedpreferences = getSharedPreferences(Second, Context.MODE_PRIVATE);


        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        btnguardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               if(!validarRegistoGoogle())
               {
                   return;
               }
                guardatos();
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(googleApiClient);
        if (opr.isDone()) {
            GoogleSignInResult result = opr.get();
            handleResult(result);
        } else {
            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(@NonNull GoogleSignInResult googleSignInResult) {
                    handleResult(googleSignInResult);
                }
            });
        }
    }

    private void handleResult (GoogleSignInResult result ) {
        if (result.isSuccess()) {
            GoogleSignInAccount account = result.getSignInAccount();

            if (account != null) {

                final String personEmail = account.getEmail();
                editemail.setText(personEmail);
                googleid           = account.getId();

                mFirebaseDatabase = mFirebaseInstance.getReference("users/" + googleid);

                mFirebaseDatabase.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        User usuario = dataSnapshot.getValue(User.class);
                        if(usuario != null)
                        {
                            String email = usuario.getEmail();

                            if(email.equals(personEmail))
                            {
                                constraintLayoutcarga.setVisibility(View.VISIBLE);
                                inputLayoutnombre.setVisibility(View.GONE);
                                inputLayoutapellido.setVisibility(View.GONE);
                                inputLayoutphone.setVisibility(View.GONE);
                                btnguardar.setVisibility(View.GONE);
                                inputLayoutemail.setVisibility(View.GONE);

                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        Intent i = new Intent(CompleteLogin.this,Config.class);
                                        i.putExtra("googleid",googleid);
                                        SharedPreferences.Editor editor = sharedpreferences.edit();
                                        editor.putString(getString(R.string.googleid),googleid);
                                        editor.apply();
                                        startActivity(i);
                                        finish();
                                    }
                                },animeTime);
                            }
                            else
                            {
                              showBottomSheet();
                              Log.d(TAG ," completarcuenta");
                            }
                        }
                        else
                        {
                            showBottomSheet();
                        }

                    }
                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Log.e(TAG , databaseError.getMessage());
                    }
                });
            }
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    public void showBottomSheet() {
        ActionBottomDialogCompleteGoogleAccount addPhotoBottomDialogFragment =
                ActionBottomDialogCompleteGoogleAccount.newInstance();
        addPhotoBottomDialogFragment.show(getSupportFragmentManager(),
                ActionBottomDialogCompleteGoogleAccount.TAG);
    }

    public boolean validarRegistoGoogle()
    {
        boolean valid = true;
        String  name  = nombre.getText().toString();
        if(TextUtils.isEmpty(name)) {

            valid = false;
            inputLayoutnombre.setError(getString(R.string.validarnombre));
            btnguardar.setBackgroundResource(R.drawable.btn_background_gray);
        }
        else
        {
            inputLayoutnombre.setError(null);
            btnguardar.setEnabled(true);
            btnguardar.setBackgroundResource(R.drawable.button_background_red);
        }
        String apellido =  lastname.getText().toString();
        if(TextUtils.isEmpty(apellido)) {
            valid = false;
            inputLayoutapellido.setError(getString(R.string.validarapellido));
            btnguardar.setBackgroundResource(R.drawable.btn_background_gray);

        }
        else {
            inputLayoutnombre.setError(null);
            btnguardar.setBackgroundResource(R.drawable.button_background_red);
        }
        String telefono= phone.getText().toString().trim();
        if(TextUtils.isEmpty(telefono)) {
            valid = false;
            inputLayoutphone.setError(getString(R.string.validartelefono));
            btnguardar.setBackgroundResource(R.drawable.btn_background_gray);
        }
        else
        {
            inputLayoutphone.setError(null);
            btnguardar.setBackgroundResource(R.drawable.button_background_red);
        }
        return valid;
    }

    public  void guardatos()
    {
        String  name        = Objects.requireNonNull(nombre.getText()).toString();
        String apellido     =  Objects.requireNonNull(lastname.getText()).toString();
        String correo       = Objects.requireNonNull(editemail.getText()).toString().trim();
        String telefono     = Objects.requireNonNull(phone.getText()).toString().trim();
        Map<String, Object> userMap = new HashMap<>();
        userMap.put(googleid,new User(name,apellido,correo,telefono));
        databaseReferencesave.updateChildren(userMap);
    }


    @Override
    public void onItemClick() {

    }
}
