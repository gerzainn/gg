package dev.com.toreritoapp;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class ActionBottomDialogUser extends BottomSheetDialogFragment implements View.OnClickListener {

    public static final String TAG = "ActionBottomDialogEmail";
    private  ActionBottomDialogUser.ItemClickListener itemClickListener;

    public static  ActionBottomDialogUser newInstance()
    {
        return new ActionBottomDialogUser();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.emailcompleterecover,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        view.findViewById(R.id.tv_mensajerecuperacion).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Button button = (Button) v;
        itemClickListener.onItemClick();
        dismiss();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        itemClickListener = null;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ActionBottomDialogUser.ItemClickListener) {
            itemClickListener = (ActionBottomDialogUser.ItemClickListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement ItemClickListener");
        }
    }

    public interface ItemClickListener {
        void onItemClick();
    }
}
