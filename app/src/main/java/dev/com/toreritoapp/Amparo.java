package dev.com.toreritoapp;

import android.content.DialogInterface;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.BillingResult;
import com.android.billingclient.api.ConsumeParams;
import com.android.billingclient.api.ConsumeResponseListener;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;
import com.android.billingclient.api.SkuDetailsResponseListener;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class Amparo extends AppCompatActivity implements  GoogleApiClient.OnConnectionFailedListener , PurchasesUpdatedListener {

    private GoogleApiClient googleApiClient;
    private GoogleSignInClient googleSignInClient;
    private TextView name;
    private TextView email;
    private FirebaseAuth firebaseAuth;
    private BillingClient mBillingClient;
    private Button btn_amparoamigo;
    private Button btnamparoatencionmedica;
    private  int codigorespuesta;
    private  int tamlista;
    private  String idgoogle;
    private  String firebaseid;
    private  String orderId;
    private String nombre_google;
    private DatabaseReference mFirebaseDatabase;
    private FirebaseDatabase mFirebaseInstance;
    private Button btnamparoalcholimetro;

    private  final static  String Tag = Amparo.class.getSimpleName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile);

        FirebaseStorage storage = FirebaseStorage.getInstance();
        btn_amparoamigo = findViewById(R.id.btnAmaparo_amigo);
        btnamparoatencionmedica= findViewById(R.id.btnAmaparoatencionmedica);
        btnamparoalcholimetro = findViewById(R.id.btnAmparo);
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        firebaseAuth= FirebaseAuth.getInstance();
        mFirebaseInstance = FirebaseDatabase.getInstance();
        mFirebaseDatabase = mFirebaseInstance.getReference("users");


        Bundle parametros = getIntent().getExtras();

        if(parametros!= null)
        {
            firebaseid = parametros.getString("firebaseId");
        }

        btnamparoatencionmedica.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AmparoAtencionMedica();
            }
        });

        btnamparoalcholimetro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AmparoAlcholiemtro();
            }
        });
        btn_amparoamigo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });


        setUpBillig();

    }

    @Override
    protected void onStart()
    {
        super.onStart();

        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(googleApiClient);
        if (opr.isDone()) {
            GoogleSignInResult result = opr.get();
            handleResult(result);
        } else {
            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(@NonNull GoogleSignInResult googleSignInResult) {
                    handleResult(googleSignInResult);
                }
            });
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    private void handleResult (GoogleSignInResult result )
    {
        if(result.isSuccess())
        {
            GoogleSignInAccount account = result.getSignInAccount();

            if(account!=null)
            {

                Log.d(Tag, "Exito al cargar el perfil");
                nombre_google= account.getDisplayName();
                idgoogle = account.getId();
                String personName = account.getDisplayName();
                String personGivenName = account.getGivenName();
                String personFamilyName = account.getFamilyName();
                String personEmail = account.getEmail();

            }
        }
    }

    public void logOut() {
            Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(new ResultCallback<Status>() {
                @Override
                public void onResult(@NonNull Status status) {
                    if (status.isSuccess())
                    {

                        goLogInScreen();
                    } else
                    {
                        Toast.makeText(getApplicationContext(), "Eror el cerrar sesión", Toast.LENGTH_SHORT).show();
                    }
                }
            });

    }

    private void goLogInScreen()
    {
        Intent intent = new Intent(this, Account.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_amparo, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        if (item.getItemId() == R.id.amparosalir) {
            DialogoCerrarsesion();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void  DialogoCerrarsesion()
    {
        AlertDialog.Builder builder= new AlertDialog.Builder(this);
        builder.setTitle(R.string.cerrarsesion);
        builder.setMessage(R.string.confirmardialogo);
        builder.setPositiveButton(R.string.aceptar, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                firebaseAuth.signOut();
                logOut();
            }
        });

        builder.setNegativeButton(R.string.cancelar, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();

    }

    public  void AmparoAtencionMedica()
    {
        Intent intent = new Intent(this, AmparoAtencionMedica.class);
        intent.putExtra("googleid",idgoogle);
        intent.putExtra("firebaseId",firebaseid);
        startActivity(intent);

    }

    public void AmparoAlcholiemtro()
    {
        Intent intent = new Intent(this,AmparoAlcholimetroProceso.class);
        intent.putExtra("googleid",idgoogle);
        intent.putExtra("firebaseId",firebaseid);
        startActivity(intent);

    }

    @Override
    public void onBackPressed() {
        DialogoCerrarsesion();
    }

    public void PagoAmparo(View view)
    {
        List<String> products = new ArrayList<>();
        products.add("amp_01");

        if(mBillingClient.isReady())
        {
            SkuDetailsParams.Builder params = SkuDetailsParams
                    .newBuilder();
            params.setSkusList(products)
                    .setType(BillingClient.SkuType.INAPP)
                    .build();
            mBillingClient.querySkuDetailsAsync(params.build(), new SkuDetailsResponseListener() {
                @Override
                public void onSkuDetailsResponse(BillingResult billingResult, List<SkuDetails> list) {

                    if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK  )
                    {
                         codigorespuesta = billingResult.getResponseCode();
                         tamlista= list.size();
                        Log.d(Tag, "Sucees " +codigorespuesta +" "+ "Tam"+ tamlista);



                        for (SkuDetails skuDetails : list)
                        {
                            String sku = skuDetails.getSku();
                            String price = skuDetails.getPrice();

                            BillingFlowParams flowParams = BillingFlowParams.newBuilder()
                                    .setSkuDetails(skuDetails)
                                    .build();
                            BillingResult responsecode = mBillingClient.launchBillingFlow(Amparo.this,flowParams);
                            int response = responsecode.getResponseCode();

                            Log.d(Tag , "precio" + price + " sku"+ " " + sku);
                            Log.d(Tag, "codigoBilligResult"+ " " + response);
                    }

                    }

                    else
                    {
                        Log.e(Tag,  String.valueOf(codigorespuesta));
                    }
                }
            });

        }
        else
        {
            Toast.makeText(getApplicationContext(),"BillingNotReady",Toast.LENGTH_LONG).show();
        }


    }


    public void setUpBillig()
    {
        mBillingClient = BillingClient
                .newBuilder(Amparo.this)
                .setListener(this)
                .enablePendingPurchases()
                .build();
        mBillingClient.startConnection(new BillingClientStateListener()
        {

            @Override
            public void onBillingSetupFinished(BillingResult billingResult) {
                if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK)
                {
                    Log.d(Tag, "RespuestaOK");

                }
                else

                {
                    Log.e(Tag , "error" + billingResult.getResponseCode());
                }
            }

            @Override
            public void onBillingServiceDisconnected() {

                Log.e(Tag ,"Desconectado");
            }
        });
    }

    public void  handlePurchase(Purchase purchase)
    {
        if(purchase!=null)
        {
            ConsumeParams consumeParams= ConsumeParams.newBuilder()
                    .setPurchaseToken(purchase.getPurchaseToken())
                    .build();

            ConsumeResponseListener consumeResponseListener= new ConsumeResponseListener() {
                @Override
                public void onConsumeResponse(BillingResult billingResult, String s) {
                    if(billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK && s!= null)
                    {
                        Log.d(Tag, "onConsumeResponseOk");
                    }
                }
            };
            mBillingClient.consumeAsync(consumeParams,consumeResponseListener);

        }
    }

    @Override
    public void onPurchasesUpdated(BillingResult billingResult, @Nullable List<Purchase> list)
    {

        if(billingResult.getResponseCode() ==  BillingClient.BillingResponseCode.OK
         && list != null)
        {
            for (Purchase purchase : list)
            {
                handlePurchase(purchase);
                orderId= purchase.getOrderId();
            }


        }
        else if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.USER_CANCELED)
        {
            Toast.makeText(getApplicationContext(), "Se cancelo la compra", Toast.LENGTH_SHORT).show();
        }
        else if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.ITEM_ALREADY_OWNED)
        {
            Toast.makeText(getApplicationContext(), "El producto ya se compro anteriormente ", Toast.LENGTH_SHORT).show();

        }
        else if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.ERROR)
        {
            Toast.makeText(getApplicationContext(),"developer error",Toast.LENGTH_SHORT).show();
        }
    }


    public void createPdf()
    {
        String directorio = "/Torerito/";
        String pdfextension = ".pdf";
        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference rootreference = storage.getReference("Users");
        DatabaseReference mFirebaseDatabase;
        FirebaseDatabase mFirebaseInstance;

        mFirebaseInstance = FirebaseDatabase.getInstance();
        mFirebaseDatabase = mFirebaseInstance.getReference("users/"+ idgoogle+"/amparo/");
        StorageReference userRef;



        String directory_path = Environment.getExternalStorageDirectory().getAbsolutePath()+ "/torerito/"+ idgoogle;
        File file = new File(directory_path);
        if(!file.exists())
        {
            file.mkdir();

        }
        String targetpdf =  directory_path+"/"+idgoogle+".pdf";
        File filePath = new File(targetpdf);
        try
        {

            SimpleDateFormat format = new SimpleDateFormat("dd 'de' MMMM 'del' yyyy", Locale.getDefault());
            String datestring = format.format(new Date());



            orderId= orderId.replaceAll("[^A-Za-z0-9]", "");
            Map<String, Object> userMap = new HashMap<>();
            mFirebaseDatabase.updateChildren(userMap);

            userRef = rootreference.child(idgoogle + "/"+filePath.getName());
            UploadTask uploadTask= userRef.putFile(Uri.fromFile(filePath));
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.e(Tag, "Storage"+ "error el subir al archivo");
                }
            })
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            Log.d(Tag, "Storage"+ "Archivo subido");
                        }
                    });

        } catch (Exception e) {
            Log.e("main", "error "+e.toString());
            Toast.makeText(this, "Something wrong: " + e.toString(),  Toast.LENGTH_LONG).show();
        }





    }


}
