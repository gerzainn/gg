package dev.com.toreritoapp;

import android.app.DatePickerDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import static android.content.Context.MODE_PRIVATE;

public class PasoTresAmparoMedico extends Fragment
{
    public static final String ARG_OBJECT = "object";
    private static final String TAG = PasoTresAmparoMedico.class.getSimpleName();
    private DatePickerDialog datePickerDialog;
    private static TextInputEditText edtfecha;
    private TextInputEditText textInputEditTextpadecimientos;
    private String fechaamparo;
    private String padecimiento;

    public static PasoTresAmparoMedico newInstance(int pos)
    {
        PasoTresAmparoMedico pruebaFragment = new PasoTresAmparoMedico();
        Bundle args = new Bundle();
        args.putInt(PasoTresAmparoMedico.ARG_OBJECT, pos);
        pruebaFragment.setArguments(args);
        return pruebaFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View  view =inflater.inflate(R.layout.pasotresamparomedico, container, false);
        edtfecha = view.findViewById(R.id.edt_fechaservicio);
        textInputEditTextpadecimientos = view.findViewById(R.id.edt_padecimientos);
        edtfecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c=Calendar.getInstance();
                int myear=c.get(Calendar.YEAR);
                int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);

                datePickerDialog=new DatePickerDialog(Objects.requireNonNull(getContext()), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        edtfecha.setText(String.format(Locale.getDefault(),"%d-% d-%d", dayOfMonth, month + 1, year));
                    }

                },myear,mMonth,mDay);
                datePickerDialog.show();
            }
        });
        return  view;
    }

    @Override
    public void onPause() {
        getDatos();
        super.onPause();

    }
    @Override
    public void onStop() {
        getDatos();
        super.onStop();
    }

    public  void getDatos()
    {
        fechaamparo = Objects.requireNonNull(edtfecha.getText()).toString();
        fechaamparo = fechaamparo.trim();
        padecimiento = Objects.requireNonNull(textInputEditTextpadecimientos.getText()).toString();
        padecimiento = padecimiento.trim();

        if(fechaamparo.isEmpty() || padecimiento.isEmpty())
        {
            Toast.makeText(getContext(),"Datos vacios",Toast.LENGTH_LONG).show();
        }
        else
        {
            ((AmparoMedicoProceso) Objects.requireNonNull(getActivity())).fechaamparo = fechaamparo;
            ((AmparoMedicoProceso) Objects.requireNonNull(getActivity())).padecimiento = padecimiento;

            String datos = String.format("Fecha :%s \n Padecimiento",fechaamparo,padecimiento);
            Log.d(TAG, datos);

        }



    }
}
