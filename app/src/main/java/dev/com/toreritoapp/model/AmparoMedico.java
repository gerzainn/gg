package dev.com.toreritoapp.model;

public class AmparoMedico {
    public String nombre;
    public String direccion;
    public String nombreinstitutomedico;
    public String direccioninstituomedico;
    public String estado;
    public String municipio;
    public String fechaatencionmedica;
    public String descripcionpadecimientos;
    public int tipo;
    public String nombretutor;
    public String nombremenor;
    public Boolean esmenor;
    public String estatus;

    public String getEstatus() {
        return estatus;
    }

    public void setEstatus(String estatus) {
        this.estatus = estatus;
    }

    public String getNombremenor() {
        return nombremenor;
    }

    public void setNombremenor(String nombremenor) {
        this.nombremenor = nombremenor;
    }

    public String getNombretutor() {
        return nombretutor;
    }

    public void setNombretutor(String nombretutor) {
        this.nombretutor = nombretutor;
    }

    public Boolean getEsmenor() {
        return esmenor;
    }

    public void setEsmenor(Boolean esmenor) {
        this.esmenor = esmenor;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getNombreinstitutomedico() {
        return nombreinstitutomedico;
    }

    public void setNombreinstitutomedico(String nombreinstitutomedico) {
        this.nombreinstitutomedico = nombreinstitutomedico;
    }

    public String getDireccioninstituomedico() {
        return direccioninstituomedico;
    }

    public void setDireccioninstituomedico(String direccioninstituomedico) {
        this.direccioninstituomedico = direccioninstituomedico;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public String getFechaatencionmedica() {
        return fechaatencionmedica;
    }

    public void setFechaatencionmedica(String fechaatencionmedica) {
        this.fechaatencionmedica = fechaatencionmedica;
    }

    public String getDescripcionpadecimientos() {
        return descripcionpadecimientos;
    }

    public void setDescripcionpadecimientos(String descripcionpadecimientos) {
        this.descripcionpadecimientos = descripcionpadecimientos;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public AmparoMedico(String nombre, String direccion, String nombreinstitutomedico, String direccioninstituomedico, String estado, String municipio,
                        String fechaatencionmedica, String descripcionpadecimientos, int tipo, Boolean esmenor, String status) {
        this.nombre = nombre;
        this.direccion = direccion;
        this.nombreinstitutomedico = nombreinstitutomedico;
        this.direccioninstituomedico = direccioninstituomedico;
        this.estado = estado;
        this.municipio = municipio;
        this.fechaatencionmedica = fechaatencionmedica;
        this.descripcionpadecimientos = descripcionpadecimientos;
        this.tipo = tipo;
        this.esmenor = esmenor;
        this.estatus = status;
    }

    public AmparoMedico(String nombretutor, String direccion, String nombremenor, String nombreinstitutomedico, String direccioninstituomedico,
                        String estado, String municipio, String fechaatencionmedica, String descripcionpadecimientos, int tipo, Boolean esmenor, String status) {
        this.nombretutor = nombretutor;
        this.direccion = direccion;
        this.nombremenor = nombremenor;
        this.nombreinstitutomedico = nombreinstitutomedico;
        this.direccioninstituomedico = direccioninstituomedico;
        this.estado = estado;
        this.municipio = municipio;
        this.fechaatencionmedica = fechaatencionmedica;
        this.descripcionpadecimientos = descripcionpadecimientos;
        this.tipo = tipo;
        this.esmenor = esmenor;
        this.estatus = status;
    }

    public AmparoMedico()
    {

    }
}


