package dev.com.toreritoapp.model;

public class AmparoAlcholimetro
{
    String nombre;
    String email;
    String telefono;
    String fecha;
    String estatus;
    int tipo;
    boolean esPersonal;
    String nombreamigo;

    public boolean isEsPersonal() {
        return esPersonal;
    }

    public void setEsPersonal(boolean esPersonal) {
        this.esPersonal = esPersonal;
    }

    public String getNombreamigo() {
        return nombreamigo;
    }

    public void setNombreamigo(String nombreamigo) {
        this.nombreamigo = nombreamigo;
    }



    public String getEstatus() {
        return estatus;
    }

    public void setEstatus(String estatus) {
        this.estatus = estatus;
    }



    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }



    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public AmparoAlcholimetro(String nombre, String email, String telefono, String fecha, int tipo,String estatus ,boolean esPersonal) {
        this.nombre = nombre;
        this.email = email;
        this.telefono = telefono;
        this.fecha = fecha;
        this.tipo = tipo;
        this.estatus= estatus;
        this.esPersonal= esPersonal;
    }

    public AmparoAlcholimetro(String nombre, String email, String telefono, String fecha, String estatus,
                              int tipo, boolean esPersonal, String nombreamigo) {
        this.nombre = nombre;
        this.email = email;
        this.telefono = telefono;
        this.fecha = fecha;
        this.estatus = estatus;
        this.tipo = tipo;
        this.esPersonal = esPersonal;
        this.nombreamigo = nombreamigo;
    }

    public AmparoAlcholimetro()
    {

    }

}
