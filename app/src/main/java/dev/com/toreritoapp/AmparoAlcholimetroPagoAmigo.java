package dev.com.toreritoapp;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.BillingResult;
import com.android.billingclient.api.ConsumeParams;
import com.android.billingclient.api.ConsumeResponseListener;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;
import com.android.billingclient.api.SkuDetailsResponseListener;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import dev.com.toreritoapp.model.AmparoAlcholimetro;

public class AmparoAlcholimetroPagoAmigo extends Fragment implements PurchasesUpdatedListener {
    public static final String ARG_OBJECT = "object";
    private BillingClient mBillingClient;
    private final static String Tag = AmparoAlcholimetroPagoAmigo.class.getSimpleName();
    private int codigorespuesta;
    private int tamlista;
    private String orderId;
    private String nombrepersona;
    private String nombreamigo;
    private String email;
    private String numero;
    private DatabaseReference mFirebaseDatabase;
    private FirebaseDatabase mFirebaseInstance;
    private TextView nombreamparo;
    private TextView precio;
    private FirebaseAuth mauth;
    private String rutactualizar;
    private ConstraintLayout contenedor;

    public static AmparoAlcholimetroPagoAmigo newInstance(int pos) {
        AmparoAlcholimetroPagoAmigo amparoMedicoPago = new AmparoAlcholimetroPagoAmigo();
        Bundle args = new Bundle();
        args.putInt(AmparoAlcholimetroPagoAmigo.ARG_OBJECT, pos);
        amparoMedicoPago.setArguments(args);

        return amparoMedicoPago;
    }

    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);
        nombrepersona = ((AmparoAlcholimetroProcesoAmigo) Objects.requireNonNull(getActivity())).nombre;
        nombreamigo = ((AmparoAlcholimetroProcesoAmigo) Objects.requireNonNull(getActivity())).nombreamigo;
        email = ((AmparoAlcholimetroProcesoAmigo) Objects.requireNonNull(getActivity())).correo;
        numero = ((AmparoAlcholimetroProcesoAmigo) getActivity()).numerotelefono;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }


    @Override
    public void onPause()
    {
        super.onPause();
        nombrepersona = ((AmparoAlcholimetroProcesoAmigo) Objects.requireNonNull(getActivity())).nombre;
        email = ((AmparoAlcholimetroProcesoAmigo) Objects.requireNonNull(getActivity())).correo;
        numero = ((AmparoAlcholimetroProcesoAmigo) getActivity()).numerotelefono;
        nombreamigo = ((AmparoAlcholimetroProcesoAmigo) Objects.requireNonNull(getActivity())).nombreamigo;

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.amparoalcholimetropago, container, false);
        Button pago = view.findViewById(R.id.pagoamparoalcholimetro);
        nombreamparo = view.findViewById(R.id.tv_productoamparoalchomimetro);
        precio = view.findViewById(R.id.tv_precioamparoalcholimetro);
        contenedor = view.findViewById(R.id.contenedor_amparoalcholimetro);
        mFirebaseInstance = FirebaseDatabase.getInstance();
        mFirebaseDatabase = mFirebaseInstance.getReference("users");
        mauth = FirebaseAuth.getInstance();

        pago.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProcesarPago();
            }
        });
        setUpBillig();
        return view;
    }

    public void getAmparo() {
        List<String> amparo = new ArrayList<>();
        amparo.add("amp_01");

        SkuDetailsParams.Builder params = SkuDetailsParams
                .newBuilder();
        params.setSkusList(amparo)
                .setType(BillingClient.SkuType.INAPP)
                .build();
        mBillingClient.querySkuDetailsAsync(params.build(), new SkuDetailsResponseListener() {
            @Override
            public void onSkuDetailsResponse(BillingResult billingResult, List<SkuDetails> list) {

                if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                    codigorespuesta = billingResult.getResponseCode();
                    tamlista = list.size();
                    Log.d(Tag, "Sucees " + codigorespuesta + " " + "Tam" + tamlista);

                    for (SkuDetails skuDetails : list) {
                        String sku = skuDetails.getDescription();
                        String price = skuDetails.getPrice();
                        nombreamparo.setText(sku);
                        precio.setText(price);
                        Log.d(Tag, "precio" + price + " sku" + " " + sku);
                    }
                } else {
                    Log.e(Tag, String.valueOf(codigorespuesta));
                }
            }
        });
    }

    public void setUpBillig() {
        mBillingClient = BillingClient
                .newBuilder(Objects.requireNonNull(getContext()))
                .setListener(this)
                .enablePendingPurchases()
                .build();
        mBillingClient.startConnection(new BillingClientStateListener() {

            @Override
            public void onBillingSetupFinished(BillingResult billingResult) {
                if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                    Log.d(Tag, "RespuestaOK");
                    getAmparo();
                } else {
                    Log.e(Tag, "error" + billingResult.getResponseCode());
                }
            }

            @Override
            public void onBillingServiceDisconnected() {

                Log.e(Tag, "Desconectado");
            }
        });
    }

    public void ProcesarPago() {
        List<String> products = new ArrayList<>();
        products.add("amp_01");


        if (mBillingClient.isReady()) {
            SkuDetailsParams.Builder params = SkuDetailsParams
                    .newBuilder();
            params.setSkusList(products)
                    .setType(BillingClient.SkuType.INAPP)
                    .build();
            mBillingClient.querySkuDetailsAsync(params.build(), new SkuDetailsResponseListener() {
                @Override
                public void onSkuDetailsResponse(BillingResult billingResult, List<SkuDetails> list) {

                    if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                        codigorespuesta = billingResult.getResponseCode();
                        tamlista = list.size();
                        Log.d(Tag, "Sucees " + codigorespuesta + " " + "Tam" + tamlista);


                        for (SkuDetails skuDetails : list) {
                            String sku = skuDetails.getSku();
                            String price = skuDetails.getPrice();


                            BillingFlowParams flowParams = BillingFlowParams.newBuilder()
                                    .setSkuDetails(skuDetails)
                                    .build();
                            BillingResult responsecode = mBillingClient.launchBillingFlow(getActivity(), flowParams);
                            int response = responsecode.getResponseCode();

                            Log.d(Tag, "precio" + price + " sku" + " " + sku);
                            Log.d(Tag, "codigoBilligResult" + " " + response);
                        }

                    } else {
                        Log.e(Tag, String.valueOf(codigorespuesta));
                    }
                }
            });

        } else {
            Toast.makeText(getContext(), "BillingNotReady", Toast.LENGTH_LONG).show();
        }
    }

    public void  DialogoAdvertencia()
    {
        AlertDialog.Builder builder= new AlertDialog.Builder(getContext());
        builder.setTitle("Amapro");
        builder.setMessage(" El amparo no puede ser procesado porque faltan datos");
        builder.setPositiveButton(R.string.aceptar, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();

            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();

    }


    public void handlePurchase(Purchase purchase) {

        if (purchase != null) {
            ConsumeParams consumeParams = ConsumeParams.newBuilder()
                    .setPurchaseToken(purchase.getPurchaseToken())
                    .build();

            ConsumeResponseListener consumeResponseListener = new ConsumeResponseListener() {
                @Override
                public void onConsumeResponse(BillingResult billingResult, String s) {
                    if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK && s != null) {
                        Log.d(Tag, "onConsumeResponseOk");
                    }
                }
            };
            mBillingClient.consumeAsync(consumeParams, consumeResponseListener);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent i = new Intent(getActivity(),CompraCompletada.class);
                    startActivity(i);
                }
            },3000L);

        }
    }

    public boolean validate() {
        boolean valid = true;

        if (TextUtils.isEmpty(nombrepersona)) {
            valid = false;
        }

        if (TextUtils.isEmpty(nombreamigo)) {
            valid = false;
        }

        if (TextUtils.isEmpty(email)) {
            valid = false;
        }

        if (TextUtils.isEmpty(numero)) {
            valid = false;
        }
        return valid;
    }

    public void styleSnackbar(View view) {
        Snackbar snackbar = Snackbar.make(view, getString(R.string.errorcompra), Snackbar.LENGTH_LONG)
                .setAction("", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                    }
                });

        //Accion
        snackbar.setActionTextColor(getResources().getColor(R.color.whiteTextColor));
        View snackBarView = snackbar.getView();
        //Fondo
        snackBarView.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        //Imagen
        TextView tv = (TextView) snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        tv.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_error, 0, 0, 0);

        snackbar.show();
    }


    @Override
    public void onPurchasesUpdated(BillingResult billingResult, @Nullable List<Purchase> list) {
        if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK
                && list != null) {
            for (Purchase purchase : list) {
                handlePurchase(purchase);
                orderId = purchase.getOrderId();
            }

            crearAmparoAmigo();
        } else if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.USER_CANCELED) {
            Toast.makeText(getContext(), "Se cancelo la compra", Toast.LENGTH_SHORT).show();
        } else if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.ITEM_ALREADY_OWNED) {
            Toast.makeText(getContext(), "El producto ya se compro anteriormente ", Toast.LENGTH_SHORT).show();

        } else if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.ERROR) {
            styleSnackbar(contenedor);
        }
    }

    public void crearAmparoAmigo()
    {
        mFirebaseInstance = FirebaseDatabase.getInstance();
        SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault());
        String datestring = format.format(new Date());


        if (mauth.getCurrentUser() != null) {

            FirebaseUser firebaseUser = mauth.getCurrentUser();
            assert firebaseUser != null;
            String personId = firebaseUser.getUid();
            mFirebaseDatabase = mFirebaseInstance.getReference("users")
                    .child(personId)
                    .child("amparos");
                orderId = orderId.replaceAll("[^A-Za-z0-9]", "");
                Map<String, Object> userMap = new HashMap<>();

                userMap.put(orderId, new AmparoAlcholimetro(
                        nombrepersona,
                        email,
                        numero,
                        datestring,
                        "Pendiente,",
                        3,
                        false,
                        nombreamigo));
                mFirebaseDatabase.updateChildren(userMap);

                mFirebaseDatabase = mFirebaseInstance.getReference("sinprocesar").push();
                Map<String ,Object> procesaramparo = new HashMap<>();
                String id = mFirebaseDatabase.getKey();
                rutactualizar= "/users/"+personId+"/amparos/"+orderId;
                procesaramparo.put("ruta",rutactualizar);
                mFirebaseDatabase.updateChildren(procesaramparo);

        } else
        {
            GoogleSignInAccount acct = GoogleSignIn.getLastSignedInAccount(Objects.requireNonNull(getActivity()));
            if (acct != null)
            {
                String googleId = acct.getId();

                mFirebaseDatabase = mFirebaseInstance.getReference("users")
                        .child(googleId)
                        .child("amparos");

                    orderId = orderId.replaceAll("[^A-Za-z0-9]", "");
                    Map<String, Object> userMap = new HashMap<>();
                    userMap.put(orderId, new AmparoAlcholimetro(
                    nombrepersona,
                    email,
                    numero,
                    datestring,
                    "Pendiente,",
                    3,
                    false,
                    nombreamigo));
                    mFirebaseDatabase.updateChildren(userMap);

                mFirebaseDatabase = mFirebaseInstance.getReference("sinprocesar").push();
                Map<String ,Object> procesaramparo = new HashMap<>();
                rutactualizar= "/users/"+googleId+"/amparos/"+orderId;
                procesaramparo.put("ruta",rutactualizar);
                mFirebaseDatabase.updateChildren(procesaramparo);
            }
        }
    }

}
