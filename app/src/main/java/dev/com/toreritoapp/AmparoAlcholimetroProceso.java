package dev.com.toreritoapp;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.BillingResult;
import com.android.billingclient.api.ConsumeParams;
import com.android.billingclient.api.ConsumeResponseListener;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;
import com.android.billingclient.api.SkuDetailsResponseListener;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import dev.com.toreritoapp.model.AmparoAlcholimetro;
import dev.com.toreritoapp.model.User;


public class AmparoAlcholimetroProceso extends FragmentActivity  implements PurchasesUpdatedListener {

    private BillingClient mBillingClient;
    private  int codigorespuesta;
    private  int tamlista;
    private  final static  String Tag = AmparoAlcholimetroProceso.class.getSimpleName();
    private  String orderId;
    private  String nombrepersona;
    private String email;
    private String numero;
    private String personId;
    private String rutactualizar;
    private DatabaseReference mFirebaseDatabase;
    private FirebaseDatabase mFirebaseInstance;
    private TextView nombreamparo;
    private TextView precio;
    private FirebaseAuth mauth;
    ConstraintLayout contenedor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.amparo_alcholimetro_proceso);

        Button pago = findViewById(R.id.pagoamparoalcholimetro);
        contenedor= findViewById(R.id.contenedoramparo);
        nombreamparo  = findViewById(R.id.tv_productoamparoalchomimetro);
        precio        = findViewById(R.id.tv_precioamparoalcholimetro);
        mFirebaseInstance = FirebaseDatabase.getInstance();
        mFirebaseDatabase = mFirebaseInstance.getReference("users");
        mauth = FirebaseAuth.getInstance();

        pago.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProcesarPago();
            }
        });

        getProfile();
        setUpBillig();
    }

    @Override
    protected void onStop() {
        super.onStop();

    }

    public void getAmparo()
    {
        List<String> amparo = new ArrayList<>();
        amparo.add("amp_01");

        SkuDetailsParams.Builder params = SkuDetailsParams
                .newBuilder();
        params.setSkusList(amparo)
                .setType(BillingClient.SkuType.INAPP)
                .build();
        mBillingClient.querySkuDetailsAsync(params.build(), new SkuDetailsResponseListener() {
            @Override
            public void onSkuDetailsResponse(BillingResult billingResult, List<SkuDetails> list) {

                if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK  )
                {
                    codigorespuesta = billingResult.getResponseCode();
                    tamlista= list.size();
                    Log.d(Tag, "Sucees " +codigorespuesta +" "+ "Tam"+ tamlista);

                    for (SkuDetails skuDetails : list)
                    {
                        String sku = skuDetails.getDescription();
                        String price = skuDetails.getPrice();
                        nombreamparo.setText(sku);
                        precio.setText(price);
                    }
                }

                else
                {
                    Log.e(Tag,  String.valueOf(codigorespuesta));
                }
            }
        });
    }

    public void  getProfile()
    {
        if (mauth.getCurrentUser() != null) {
            FirebaseUser firebaseUser = mauth.getCurrentUser();
            assert firebaseUser != null;
            personId = firebaseUser.getUid();


            mFirebaseDatabase = mFirebaseInstance.getReference("users/" + personId);

            mFirebaseDatabase.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    User usuario = dataSnapshot.getValue(User.class);

                    if (usuario != null) {
                         nombrepersona = usuario.getFirstname();
                         email = usuario.getEmail();
                         numero = usuario.getPhone();


                    }

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        } else
            {
                GoogleSignInAccount acct = GoogleSignIn.getLastSignedInAccount(Objects.requireNonNull(getApplication()));
                if (acct != null) {

                    String googleId = acct.getId();

                    mFirebaseDatabase = mFirebaseInstance.getReference("users/" + googleId);
                    mFirebaseDatabase.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            User usuario = dataSnapshot.getValue(User.class);

                            if (usuario != null) {
                                nombrepersona = usuario.getFirstname();
                                email = usuario.getEmail();
                                numero = usuario.getPhone();


                            }

                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                }
        }
    }

        public void setUpBillig()
    {
        mBillingClient = BillingClient
                .newBuilder(getApplicationContext())
                .setListener(this)
                .enablePendingPurchases()
                .build();
        mBillingClient.startConnection(new BillingClientStateListener()
        {

            @Override
            public void onBillingSetupFinished(BillingResult billingResult) {
                if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK)
                {
                    Log.d(Tag, "RespuestaOK");
                    getAmparo();
                }
                else

                {
                    Log.e(Tag , "error" + billingResult.getResponseCode());
                }
            }

            @Override
            public void onBillingServiceDisconnected() {

                Log.e(Tag ,"Desconectado");
            }
        });
    }


    public void ProcesarPago()
    {
        List<String> products = new ArrayList<>();
        products.add("amp_01");


        if(mBillingClient.isReady())
        {
            SkuDetailsParams.Builder params = SkuDetailsParams
                    .newBuilder();
            params.setSkusList(products)
                    .setType(BillingClient.SkuType.INAPP)
                    .build();
            mBillingClient.querySkuDetailsAsync(params.build(), new SkuDetailsResponseListener() {
                @Override
                public void onSkuDetailsResponse(BillingResult billingResult, List<SkuDetails> list) {

                    if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK  )
                    {
                        codigorespuesta = billingResult.getResponseCode();
                        tamlista= list.size();
                        Log.d(Tag, "Sucees " +codigorespuesta +" "+ "Tam"+ tamlista);



                        for (SkuDetails skuDetails : list)
                        {
                            String sku = skuDetails.getSku();
                            String price = skuDetails.getPrice();

                            BillingFlowParams flowParams = BillingFlowParams.newBuilder()
                                    .setSkuDetails(skuDetails)
                                    .build();
                            BillingResult responsecode = mBillingClient.launchBillingFlow(AmparoAlcholimetroProceso.this,flowParams);
                            int response = responsecode.getResponseCode();

                            Log.d(Tag, "codigoBilligResult"+ " " + response);
                        }

                    }

                    else
                    {
                        Log.e(Tag,  String.valueOf(codigorespuesta));
                    }
                }
            });

        }
        else
        {
            Toast.makeText(getApplicationContext(),"BillingNotReady",Toast.LENGTH_LONG).show();
        }
    }

    public void  handlePurchase(Purchase purchase)
    {

        if(purchase!=null)
        {
            ConsumeParams consumeParams= ConsumeParams.newBuilder()
                    .setPurchaseToken(purchase.getPurchaseToken())
                    .build();

            ConsumeResponseListener consumeResponseListener= new ConsumeResponseListener() {
                @Override
                public void onConsumeResponse(BillingResult billingResult, String s) {
                    if(billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK && s!= null)
                    {
                        Log.d(Tag, "onConsumeResponseOk");
                    }

                }
            };


            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent i = new Intent(getApplicationContext(),CompraCompletada.class);
                    startActivity(i);
                    finish();
                }
            },3000L);
            mBillingClient.consumeAsync(consumeParams,consumeResponseListener);

        }
    }

    public void styleSnackbar(View view) {
        Snackbar snackbar = Snackbar.make(view, getString(R.string.errorcompra), Snackbar.LENGTH_LONG)
                .setAction("", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                    }
                });

        //Accion
        snackbar.setActionTextColor(getResources().getColor(R.color.whiteTextColor));
        View snackBarView = snackbar.getView();
        //Fondo
        snackBarView.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        //Imagen
        TextView tv = (TextView) snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        tv.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_error, 0, 0, 0);

        snackbar.show();
    }

    @Override
    public void onPurchasesUpdated(BillingResult billingResult, @Nullable List<Purchase> list) {
        codigorespuesta = billingResult.getResponseCode();

        if(billingResult.getResponseCode() ==  BillingClient.BillingResponseCode.OK
                && list != null)
        {
            for (Purchase purchase : list)
            {
                handlePurchase(purchase);
                orderId= purchase.getOrderId();
            }

            crearAmparoAlcholimetro();
        }
        else if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.USER_CANCELED)
        {
            Toast.makeText(getApplicationContext(), "Se cancelo la compra", Toast.LENGTH_SHORT).show();
        }
        else if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.ITEM_ALREADY_OWNED)
        {
            Toast.makeText(getApplicationContext(), "El producto ya se compro anteriormente ", Toast.LENGTH_SHORT).show();

        }
        else if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.ERROR)
        {
          styleSnackbar(contenedor);
        }
    }

    public void crearAmparoAlcholimetro()
    {
        mFirebaseInstance = FirebaseDatabase.getInstance();
        SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault());
        String datestring = format.format(new Date());


        if(mauth.getCurrentUser() != null)

        {

            FirebaseUser firebaseUser = mauth.getCurrentUser();
            assert firebaseUser != null;
            String personId = firebaseUser.getUid();
            mFirebaseDatabase = mFirebaseInstance.getReference("users")
                    .child(personId)
                    .child("amparos");
            orderId= orderId.replaceAll("[^A-Za-z0-9]", "");
            Map<String, Object> userMap = new HashMap<>();

            userMap.put(orderId, new AmparoAlcholimetro(
                    nombrepersona,
                    email,
                    numero,
                    datestring,
                    2,
                    "Pendiente",
                    true));
            mFirebaseDatabase.updateChildren(userMap);

            mFirebaseDatabase = mFirebaseInstance.getReference("sinprocesar").push();
            Map<String ,Object> procesaramparo = new HashMap<>();
            String id = mFirebaseDatabase.getKey();
            rutactualizar= "/users/"+personId+"/amparos/"+orderId;
            procesaramparo.put("ruta",rutactualizar);
            mFirebaseDatabase.updateChildren(procesaramparo);

        }
        else
        {
            GoogleSignInAccount acct = GoogleSignIn.getLastSignedInAccount(Objects.requireNonNull(getApplicationContext()));
            if (acct != null)
            {
                String googleId = acct.getId();

                mFirebaseDatabase = mFirebaseInstance.getReference("users")
                        .child(googleId)
                        .child("amparos");

                orderId= orderId.replaceAll("[^A-Za-z0-9]", "");
                Map<String, Object> userMap = new HashMap<>();

                userMap.put(orderId, new AmparoAlcholimetro(
                        nombrepersona,
                        email,
                        numero,
                        datestring,
                        2,
                        "Pendiente",
                        true));
                mFirebaseDatabase.updateChildren(userMap);

                mFirebaseDatabase = mFirebaseInstance.getReference("sinprocesar").push();
                Map<String ,Object> procesaramparo = new HashMap<>();
                String id = mFirebaseDatabase.getKey();
                rutactualizar= "/users/"+googleId+"/amparos/"+orderId;
                procesaramparo.put("ruta",rutactualizar);
                mFirebaseDatabase.updateChildren(procesaramparo);
            }
        }
    }

}