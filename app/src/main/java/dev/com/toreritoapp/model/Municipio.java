package dev.com.toreritoapp.model;

import com.squareup.moshi.Json;

public class Municipio implements  Comparable<Municipio> {
    @Json(name = ("id"))
    public int idMunicipio;
    @Json(name = ("estado_id"))
    public int estado_id;

    @Override
    public String toString() {
        return nombre;

    }
    @Json(name = ("nombre"))
    public String nombre;

    public Municipio(int idMunicipio, int estado_id, String nombre) {
        this.idMunicipio = idMunicipio;
        this.estado_id = estado_id;
        this.nombre = nombre;
    }

    public int getEstado_id() {
        return estado_id;
    }

    public void setEstado_id(int estado_id) {
        this.estado_id = estado_id;
    }

    public int getIdMunicipio() {
        return idMunicipio;
    }

    public void setIdMunicipio(int idMunicipio) {
        this.idMunicipio = idMunicipio;
    }



    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }



    @Override
    public int compareTo(Municipio municipio) {
        return this.getIdMunicipio()- municipio.getIdMunicipio();
    }
}
